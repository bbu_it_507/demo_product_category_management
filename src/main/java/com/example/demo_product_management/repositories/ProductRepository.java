package com.example.demo_product_management.repositories;

import com.example.demo_product_management.models.Category;
import com.example.demo_product_management.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product , Integer> {
   Product  findByIdAndStatus(Integer id, String status);
   Product findAllById(Integer id);
}
