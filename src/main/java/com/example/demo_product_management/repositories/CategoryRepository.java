package com.example.demo_product_management.repositories;

import com.example.demo_product_management.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Category  findByIdAndStatus(Integer id, String status);
}
