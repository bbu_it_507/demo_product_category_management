package com.example.demo_product_management.controllers;

import com.example.demo_product_management.models.Product;
import com.example.demo_product_management.repositories.CategoryRepository;
import com.example.demo_product_management.repositories.ProductRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ProductController {
    final ProductRepository productRepository;
    final CategoryRepository categoryRepository;


    public ProductController(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }
    @GetMapping("/products")
    public String index (Model model){
        model.addAttribute("products",productRepository.findAll());
        System.out.println(productRepository.findAll());
        return "product/index";
    }
    @GetMapping("/products/create")
    public String create (Model model){
        model.addAttribute("product", new Product());
        return "product/create";
    }
    @PostMapping("/products/create")
    public String create (Model model,@ModelAttribute("product") Product product){
        Product product1 = new Product();
        product1.setCategories(categoryRepository.findAll());
//        model.addAttribute("products",productRepository.findById(product.id));
        productRepository.save(product1);
        return "redirect:/products";
    }
    @GetMapping("/products/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model) {
        Product product = productRepository.findById(id).orElse(null);
        if (product != null) {
            model.addAttribute("product", product);
            return "product/create";
        }
        return "redirect:/products";

    }
    @GetMapping("/products/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        Product find = productRepository.findById(id).orElse(null);
        if(find != null) {
            find.setId(id);
            productRepository.delete(find);
        }
        return "redirect:/products";
    }
}
