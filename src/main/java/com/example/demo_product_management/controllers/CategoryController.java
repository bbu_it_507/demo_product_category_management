package com.example.demo_product_management.controllers;

import com.example.demo_product_management.models.Category;
import com.example.demo_product_management.repositories.CategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CategoryController {
    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/categories")
    public String index(Model model){
        model.addAttribute("categories", categoryRepository.findAll());
        System.out.println(categoryRepository.findAll());
        return "category/index";
    }
    @GetMapping("/categories/create")
    public String create (Model model){
        model.addAttribute("category" ,new Category());
        return "category/create";
    }

    @PostMapping ("/categories/create")
    public String create (Model model, @ModelAttribute("category") Category category){
        categoryRepository.save(category);
        return "redirect:/categories";
    }
    @GetMapping("/categories/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model){
        Category category = categoryRepository.findById(id).orElse(null);
        if(category != null) {
            model.addAttribute("category", category);
            return "category/create";
        }
        return "redirect:/categories";
    }
    @GetMapping("/categories/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        Category find = categoryRepository.findById(id).orElse(null);
        if(find != null) {
            find.setStatus("DELETE");
            categoryRepository.save(find);
        }
        return "redirect:/categories";
    }
    @GetMapping("/categories/active/{id}")
    public String delete(@PathVariable("id") Integer id ,Model model ){
        model.addAttribute("catagories" , new Category());
        Category find = categoryRepository.findById(id).orElse(null);
        if(find != null) {
            find.setStatus("ACTIVE");
            categoryRepository.save(find);
        }
        return "redirect:/categories";
    }
}
