package com.example.demo_product_management.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "products")
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    public String name;
    public int code;
    public int barcode;
    public double cost;
    public double price;
    public String stocktype;
    public String status;
    @ManyToOne
    private Category category;
    @Transient
    private List<Category> categories;
}
