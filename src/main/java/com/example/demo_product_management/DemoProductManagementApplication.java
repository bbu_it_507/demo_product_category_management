package com.example.demo_product_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoProductManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoProductManagementApplication.class, args);
    }

}
